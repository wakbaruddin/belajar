<?php
function PostData($url, $post, $bearer) {
	$headers = [
    "Authorization: bearer $bearer",
	"Content-Type: application/json; charset=utf-8"
];
    $ch = curl_init($url);
	
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 30000);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	if( ! $result = curl_exec($ch))
    {
        trigger_error(curl_error($ch));
    }
    curl_close($ch); 
    return $result;
}

$userId=0;
if(isset($_GET['userId'])){
	$userId = $_GET['userId'];
}
$bearer='';
if(isset($_GET['bearer'])){
	$bearer = $_GET['bearer'];
}
$data = PostData('https://api.anis.web.id/user/printSuratMandat',"{\"userId\":$userId}",$bearer);
//$data2 = curl_post('https://api.anis.web.id/user/printSuratMandat','{"userId":151}');


//echo $data;
$objData=json_decode($data,true);

function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}
 
$tgl = tgl_indo(date('Y-m-d')); 

$path = '../img/Cat03.jpg';
$type = pathinfo($path, PATHINFO_EXTENSION);
$data = file_get_contents($path);
$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
?>
    <style>
        @page {
            size: A4;
            margin: 20mm;
        }

        body {
            line-height: 1.5;
            font-family: Arial, sans-serif;
            font-size: 13px;
        }

        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            box-sizing: border-box;
            page-break-after: always;
        }

        p {
            margin: 12px 0;
        }

        table {
            border-collapse: collapse;
            width: 100%;
            font-family: Arial, sans-serif;
            font-size: 13px;
        }

        table, th, td {
            padding: 12px;
            border: none;
        }

        th, td {
            padding: 10px;
            text-align: left;
        }

        .center {
            text-align: right;
        }

        .center-align {
            text-align: center;
            /* text-decoration: underline; */
        }
    </style>
    <div class="sheet">
        <h2 class="center-align">
            <strong><u>SURAT MANDAT SAKSI</u></strong>
        </h2><br/>
		<!--<img src="<?php echo $base64;?>" alt="image" >-->
        <p>Saya yang bertandatangan di bawah ini:</p>
        <table>
            <tbody>
                <tr>
                    <td>Nama Lengkap</td>
                    <td>: Uryan Riana</td>
                </tr>
                <tr>
                    <td>Jabatan</td>
                    <td>: Ketua Tim Kampanye Daerah (TKD) Kabupaten Bekasi</td>
                </tr>
                <tr>
                    <td>Alamat Sekretariat</td>
                    <td>: Ruko Sentra Niaga Kalimas, Jl. kiyai H. Noer Ali No.16 blok A, Setiadarma, Kec. Tambun Sel., Kabupaten Bekasi, Jawa Barat 17510</td>
                </tr>
            </tbody>
        </table>

        <p>
            Dengan ini memberikan <strong>MANDAT</strong> menjadi <strong>SAKSI</strong> dari pasangan H. Anies Rasyid Baswedan, S.E., M.P.P., Ph.D. dan Dr. (HC) Drs. HA Muhaimin Iskandar, M.Si. (calon Presiden dan Wakil Presiden No. Urut 1), pada pemilu Presiden dan Wakil Presiden 2024, kepada:
        </p>

        <table>
            <tbody>
                <tr>
                    <td>Nama Lengkap</td>
                    <td>: <?php echo $objData['nama']; ?></td>
                </tr>
                <tr>
                    <td>Bertugas pada</td>
                    <td>: No. TPS: </td>
                </tr>
                <tr>
                    <td>Desa/Kelurahan</td>
                    <td>: <?php echo $objData['id_kelurahan_name']; ?></td>
                </tr>
                <tr>
                    <td>Kecamatan</td>
                    <td>: <?php echo $objData['id_kecamatan_name']; ?></td>
                </tr>
                <tr>
                    <td>Kabupaten/Kota</td>
                    <td>: <?php echo $objData['id_kota_name']; ?></td>
                </tr>
                <tr>
                    <td>No. Telp/HP</td>
                    <td>: <?php echo $objData['phone_number']; ?></td>
                </tr>
            </tbody>
        </table>

        <p>
            Agar saksi melaksanakan mandat ini untuk menjaga proses pemungutan dan penghitungan suara sesuai dengan peraturan perundang-undangan yang berlaku.
        </p>

        <p>
            Demikian Surat Mandat Saksi ini dibuat untuk dapat dipergunakan sebagaimana mestinya.
        </p><br/>

        <p class="center">Jakarta, <?php echo $tgl;?></p>

        <table>
            <tbody>
                <tr>
                    <td class="center-align">Pemberi Mandat,</td>
                    <td class="center-align">Penerima Mandat,</td>
                </tr>
            </br>
        </br>
                <tr>
                    <td class="center-align">(Uryan Riana)</td>
                    <td class="center-align">(<?php echo $objData['nama']; ?>)</td>
                </tr>
            </tbody>
        </table>
    </div>