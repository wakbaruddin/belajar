<?php

function GetData($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 30000);
    $result = curl_exec($ch);
    curl_close($ch);  // Seems like good practice
    return $result;
}

require_once '../vendor/dompdf/src/FontLib/src/FontLib/BinaryStream.php';
require_once '../vendor/dompdf/src/FontLib/src/FontLib/Table/Type/nameRecord.php';
require_once '../vendor/dompdf/src/FontLib/src/FontLib/Table/Table.php';
require_once '../vendor/dompdf/src/FontLib/src/FontLib/Table/Type/name.php';
require_once '../vendor/dompdf/src/FontLib/src/FontLib/Table/DirectoryEntry.php';
require_once '../vendor/dompdf/src/FontLib/src/FontLib/Header.php';
require_once '../vendor/dompdf/src/FontLib/src/FontLib/TrueType/TableDirectoryEntry.php';
require_once '../vendor/dompdf/src/FontLib/src/FontLib/TrueType/Header.php';
require_once '../vendor/dompdf/src/FontLib/src/FontLib/TrueType/File.php';
require_once '../vendor/dompdf/src/FontLib/src/FontLib/Font.php';
require_once '../vendor/dompdf/src/JavascriptEmbedder.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Tag/AbstractTag.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Tag/Shape.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Tag/Line.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Tag/Rect.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Tag/AbstractTag.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Tag/Group.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Style.php';
require_once '../vendor/dompdf/src/svg/src/Svg/DefaultStyle.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Surface/SurfaceInterface.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Surface/SurfaceCpdf.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Tag/AbstractTag.php';
require_once '../vendor/dompdf/src/svg/src/Svg/CssLength.php';
require_once '../vendor/dompdf/src/svg/src/Svg/Document.php';
require_once '../vendor/dompdf/src/Cellmap.php';
require_once '../vendor/dompdf/src/Exception.php';
require_once '../vendor/dompdf/src/Exception/ImageException.php';
require_once '../vendor/dompdf/src/Image/Cache.php';
require_once '../vendor/dompdf/src/Renderer/AbstractRenderer.php';
require_once '../vendor/dompdf/src/Renderer/Block.php';
require_once '../vendor/dompdf/src/Renderer/TableCell.php';
require_once '../vendor/dompdf/src/Renderer/TableRow.php';
require_once '../vendor/dompdf/src/Renderer/TableRowGroup.php';
require_once '../vendor/dompdf/src/Renderer/ListBullet.php';
require_once '../vendor/dompdf/src/Renderer/Inline.php';
require_once '../vendor/dompdf/src/Renderer/Image.php';
require_once '../vendor/dompdf/src/Renderer/Text.php';
require_once '../vendor/dompdf/src/Renderer.php';
require_once '../vendor/dompdf/src/Helpers.php';
require_once '../vendor/dompdf/src/options.php';
require_once '../vendor/dompdf/src/Positioner/AbstractPositioner.php';
require_once '../vendor/dompdf/src/Positioner/TableCell.php';
require_once '../vendor/dompdf/src/Positioner/NullPositioner.php';
require_once '../vendor/dompdf/src/Positioner/ListBullet.php';
require_once '../vendor/dompdf/src/Positioner/Inline.php';
require_once '../vendor/dompdf/src/Positioner/Block.php';
require_once '../vendor/dompdf/src/LineBox.php';
require_once '../vendor/dompdf/src/Frame.php';
require_once '../vendor/dompdf/src/FrameReflower/AbstractFrameReflower.php';
require_once '../vendor/dompdf/src/FrameReflower/NullFrameReflower.php';
require_once '../vendor/dompdf/src/FrameReflower/Block.php';
require_once '../vendor/dompdf/src/FrameReflower/TableCell.php';
require_once '../vendor/dompdf/src/FrameReflower/TableRow.php';
require_once '../vendor/dompdf/src/FrameReflower/TableRowGroup.php';
require_once '../vendor/dompdf/src/FrameReflower/Table.php';
require_once '../vendor/dompdf/src/FrameReflower/ListBullet.php';
require_once '../vendor/dompdf/src/FrameReflower/Inline.php';
require_once '../vendor/dompdf/src/FrameReflower/Image.php';
require_once '../vendor/dompdf/src/FrameReflower/Text.php';
require_once '../vendor/dompdf/src/FrameReflower/Page.php';
require_once '../vendor/dompdf/src/FrameDecorator/AbstractFrameDecorator.php';
require_once '../vendor/dompdf/src/FrameDecorator/NullFrameDecorator.php';
require_once '../vendor/dompdf/src/FrameDecorator/Block.php';
require_once '../vendor/dompdf/src/FrameDecorator/TableCell.php';
require_once '../vendor/dompdf/src/FrameDecorator/TableRow.php';
require_once '../vendor/dompdf/src/FrameDecorator/TableRowGroup.php';
require_once '../vendor/dompdf/src/FrameDecorator/Table.php';
require_once '../vendor/dompdf/src/FrameDecorator/ListBullet.php';
require_once '../vendor/dompdf/src/FrameDecorator/Inline.php';
require_once '../vendor/dompdf/src/FrameDecorator/Text.php';
require_once '../vendor/dompdf/src/FrameDecorator/Page.php';
require_once '../vendor/dompdf/src/Frame/FrameListIterator.php';
require_once '../vendor/dompdf/src/Frame/FrameTree.php';
require_once '../vendor/dompdf/src/FrameDecorator/Image.php';
require_once '../vendor/dompdf/src/Frame/Factory.php';
require_once '../vendor/dompdf/src/Frame/FrameTreeIterator.php';
require_once '../vendor/dompdf/src/FontMetrics.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Serializer/Traverser.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Serializer/RulesInterface.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Serializer/OutputRules.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Entities.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Elements.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Parser/Tokenizer.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Parser/UTF8Utils.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Parser/CharacterReference.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Parser/Scanner.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Parser/TreeBuildingRules.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Parser/EventHandler.php';
require_once '../vendor/dompdf/src/Masterminds/src/HTML5/Parser/DOMTreeBuilder.php';
require_once '../vendor/dompdf/src/Css/Content/ContentPart.php';
require_once '../vendor/dompdf/src/Css/Content/StringPart.php';
require_once '../vendor/dompdf/src/Css/Content/Attr.php';
require_once '../vendor/dompdf/src/Css/Content/CloseQuote.php';
require_once '../vendor/dompdf/src/Css/Content/ContentPart.php';
require_once '../vendor/dompdf/src/Css/Content/OpenQuote.php';
require_once '../vendor/dompdf/src/Css/Color.php';
require_once '../vendor/dompdf/src/Css/AttributeTranslator.php';
require_once '../vendor/dompdf/src/Css/Style.php';
require_once '../vendor/dompdf/src/Css/Stylesheet.php';
require_once '../vendor/dompdf/src/Canvas.php';
require_once '../vendor/dompdf/lib/Cpdf.php';
require_once '../vendor/dompdf/src/CanvasFactory.php';
require_once '../vendor/dompdf/src/Adapter/CPDF.php';
require_once '../vendor/dompdf/src/dompdf.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;

$userId=0;
if(isset($_GET['userId'])){
	$userId = $_GET['userId'];
}
$bearer='';
if(isset($_GET['bearer'])){
	$bearer = $_GET['bearer'];
}

$host=(empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]";

$test = GetData("$host/web/user/surat-mandat.php?userId=$userId&bearer=$bearer");
// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($test);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();
$dompdf->addInfo("Surat Pernyataan","Pernyataan");

// Output the generated PDF to Browser
$dompdf->stream("invoice.pdf",["Attachment"=>0]);


?>