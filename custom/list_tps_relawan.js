$('#tpsRelawan').DataTable({
	responsive: true,
    pageLength:50,
    sPaginationType: "full_numbers",
    oLanguage: {
        oPaginate: {
            sFirst: "<<",
            sPrevious: "<",
            sNext: ">", 
            sLast: ">>" 
        }
    },
	columns: [
		{ title: 'NO TPS', data: 'tps_no' },
		{ title: 'NAMA SAKSI', data: 'name' },
		{ title: 'NO. TELEPON', data: 'phone_number' },
		{ title: 'ALAMAT TPS', data: 'tps_addr' },
		{ title: 'KELURAHAN', data: 'village_name' },
		{ title: 'KECAMATAN', data: 'district_name' },
		{ title: 'KOTA', data: 'city_name' }
	],
	dom: 'Bfrtip',
		buttons: [
			{
				extend: 'copyHtml5',
				text: '<i class="fas fa-copy"></i> Copy',
				titleAttr: 'Copy to Clipboard',
				className: 'btn btn-success'
			},
			{
				extend: 'excelHtml5',
				text: '<i class="fas fa-file-excel"></i> Excel',
				titleAttr: 'Export to Excel',
				className: 'btn btn-info'
			},
			{
				extend: 'csvHtml5',
				text: '<i class="fas fa-file-csv"></i> CSV',
				titleAttr: 'Export to CSV',
				className: 'btn btn-warning'
			},
			{
				extend: 'pdfHtml5',
				text: '<i class="fas fa-file-pdf"></i> PDF',
				titleAttr: 'Export to PDF',
				className: 'btn btn-danger'
		    }
		],
	
});

$(document).ready(function () {

	getTampil_kota(data.id_kota);
	getTampil_kecamatan(data.id_kota);
	getTampil_kelurahan(data.id_kecamatan);
});

function onchange_Kecamatan(){
	var selectedKecamatan = $('#id_kecamatan').val();
	getTampil_kelurahan(selectedKecamatan);
}

function onchange_Kelurahan(){
	getListTPSRelawan();
}

function getTampil_kota(id_kota){

	$.ajax({
        type: 'POST',
		url: apiDomain + "master/getDataKota",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},
			data: {"id_kota": id_kota},
			success: function (datas) {
					
				
				$('#id_kota').empty();
				$('#id_kota').append("<option value='"+ datas.data.KOTA_ID +"'selected>" + datas.data.NAMA_KOTA + '</option>');
				$('#id_kota').val(datas.data.KOTA_ID);		

			},

		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}

function getTampil_kecamatan(id_kota) {

	// var semuaKecamatan = document.getElementById();

	$.ajax({
        type: 'POST',
		url: apiDomain + "master/getDataKecamatan",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},
			data: {"idkota": id_kota},
			success: function (datas) {
				rows = datas.data;
				
				$('#id_kecamatan').empty();
				
				
				$('#id_kecamatan').append("<option value='"+ datas.KECAMATAN_ID +"' selected> Semua Kecamatan" + '</option>');

				console.log("<option value='"+ datas.data.KECAMATAN_ID+"' selected> Semua Kecamatan" + '</option>');
				
				if (rows.length > 0) {
					for (let index = 0; index < rows.length; index++) {					
						if (data.id_kecamatan == rows[index].KECAMATAN_ID) {
							$('#id_kecamatan').append("<option value='"+ rows[index].KECAMATAN_ID+"' selected>" + rows[index].NAMA_KECAMATAN + '</option>');

						} else {
							$('#id_kecamatan').append("<option value='"+ rows[index].KECAMATAN_ID+"'>" + rows[index].NAMA_KECAMATAN + '</option>');
						}
					}
				}
			},
	
        
		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}

function getTampil_kelurahan(id_kecamatan) {
	$.ajax({
        type: 'POST',
		url: apiDomain + "master/getDataKelurahan",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},
			data: {"idkecamatan": id_kecamatan},
			success: function (datas) {
				rows = datas.data;
				$('#id_kelurahan').empty();	
				// $('#id_kecamatan').append('<option value=""> pilih kelurahan </option>');
				if (rows.length > 0) {							
					var selectedKecamatan = $('#id_kecamatan').val();					
					for (let index = 0; index < rows.length; index++) {
							if (data.id_kelurahan == rows[index].KELURAHAN_ID && selectedKecamatan != "") {
								$('#id_kelurahan').append("<option value='"+ rows[index].KELURAHAN_ID+"' selected>" + rows[index].NAMA_KELURAHAN + '</option>');

							} else {
								$('#id_kelurahan').append("<option value='"+ rows[index].KELURAHAN_ID+"'>" + rows[index].NAMA_KELURAHAN + '</option>');
							}
						}
				}
				getListTPSRelawan();
			},
	
        
		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}

// function changeType(){
// 	var tipenya = document.getElementById("initType").value;
// 	getListTPSRelawan(tipenya);
// }

// function getListTPSRelawan(typenya){
function getListTPSRelawan(){

	var selectedKota = $('#id_kota').val();
	var selectedKecamatan = $('#id_kecamatan').val();
	var selectedKelurahan = $('#id_kelurahan').val();
	
	$.ajax({
        type: 'POST',
		// url: apiDomain + "profile/getTPSStatus/" + data.id + "/" + typenya,
		url: apiDomain + "list/tps",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},

			data: {
				kota_id: selectedKota,
				kecamatan_id: selectedKecamatan,
				kelurahan_id: selectedKelurahan
			},
			success: function (data) {

				// console.log(data);
					var TPSrelawanData = data.data;

				// console.log (TPSrelawanData);
				
				$('#tpsRelawan tbody').empty();

				var table = $('#tpsRelawan').DataTable();
				// Hapus table yang ada
				table.clear().draw();
				
				TPSrelawanData.forEach(function (TPSrel) {
					table.row.add( {
						"tps_no":   TPSrel.tps_no,
						"name":       TPSrel.name,
						"phone_number":       TPSrel.phone_number,
						"tps_addr":     TPSrel.tps_addr,
						"village_name":     TPSrel.village_name,
						"district_name": TPSrel.district_name,
						"city_name":	TPSrel.city_name
					} ).draw();
				});
			},
        
		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
	
}

