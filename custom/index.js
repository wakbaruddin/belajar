$(document).ready(function () {

    $('#dataTables-example').DataTable({
        responsive: false,
        pageLength: 25,
        sPaginationType: "full_numbers",
        oLanguage: {
            oPaginate: {
                sFirst: "<<",
                sPrevious: "<",
                sNext: ">",
                sLast: ">>"
            }
        },
        "columns": [
            { data: "no", title: "" },
            { data: "ID_RELAWAN", title: "ID Relawan", visible: false },
            { data: "NAMA_RELAWAN", title: "Nama Relawan" },
            { data: "REGISTER_DATE", title: "Tanggal Daftar" },
            { data: "NO_TELP", title: "No. Telepon" },
            { data: "NAMA_KELURAHAN", title: "Kelurahan" },
            { data: "alamat", title: "Alamat" },
            { data: "GenerateSuratMandat", title: "Surat Mandat" },
            { data: "EditButton",
                title: "Aksi",
                render: function (data, type, row) {
                    return '<button type="button" class="btn btn-primary btn-edit" data-id="' + row.ID_RELAWAN + '">Edit</button><button type="button" class="btn btn-primary btn-delete" data-id="' + row.ID_RELAWAN + '">Delete</button>';
                }
            },
        ],
        "columnDefs": [
            {
                'targets': 7,
                'data': null,
                'searchable': false,
                'orderable': false,
                'defaultContent': '<button type="button" class="btn btn-primary btn-surat">Surat Mandat</button>'
            }
        ],
        order: [[3, 'desc']],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                text: '<i class="fas fa-copy"></i> Copy',
                titleAttr: 'Copy to Clipboard',
                className: 'btn btn-success',
                exportOptions: {
                    columns: [0, 2, 3, 4, 5, 6]
                }
            },
            {
                extend: 'excelHtml5',
                text: '<i class="fas fa-file-excel"></i> Excel',
                titleAttr: 'Export to Excel',
                className: 'btn btn-info',
                exportOptions: {
                    columns: [0, 2, 3, 4, 5, 6]
                }
            },
            {
                extend: 'csvHtml5',
                text: '<i class="fas fa-file-csv"></i> CSV',
                titleAttr: 'Export to CSV',
                className: 'btn btn-warning',
                exportOptions: {
                    columns: [0, 2, 3, 4, 5, 6]
                }
            },
            {
                extend: 'pdfHtml5',
                text: '<i class="fas fa-file-pdf"></i> PDF',
                titleAttr: 'Export to PDF',
                className: 'btn btn-danger',
                exportOptions: {
                        columns: [0, 2, 3, 4, 5 ,6]
                    }
            }
        ],
    
    
    
    });
    
        getDataKota(data.id_kota);
        // getDataKecamatan(data.id_kota);
        // getDataKelurahan(data.id_kecamatan);
    
    });
    function buttonAdd_clicked(){
        // alert("add");
        var selectedKelurahan = $('#id_kelurahan').val();
        window.location.href = "form_detail_relawan.html?id=-&kelurahanId=" + selectedKelurahan;
    }
    
    
    function onchange_Kecamatan(){
        var x = document.getElementById("id_kecamatan").value;
        getDataKelurahan(x);
    }
    
    function onchange_Kelurahan() {
        getRelawanList();
    
    }
    
    function getDataKota(id_kota){
        $.ajax({
            type: 'POST',
            url: apiDomain + "master/getDataKota",
            headers: {
                Authorization: 'bearer ' + data.accessToken
            },
            data: {"id_kota": id_kota},
            success: function (dataa) {
                console.log(dataa);
                $('#id_kota').empty();
                $('#id_kota').append("<option value='"+ dataa.data.KOTA_ID +"'selected>" + dataa.data.NAMA_KOTA + '</option>');
                $('#id_kota').val(dataa.data.KOTA_ID);
                getDataKecamatan(data.id_kota);
            },
            statusCode: {
                404: function() {
                  console.log( "page not found" );
                }
            }
        })
        .done(function() {
            console.log( "success" );
        })
        .fail(function() {
            console.log( "error" );
        })
        .always(function() {
            console.log( "complete" );
        });
    }
    
    function getDataKecamatan(id_kota) {
        // console.log(id_kota);
    
        $.ajax({
            type: 'POST',
            url: apiDomain + 'master/getDataKecamatan',
            headers: {
                Authorization: 'bearer ' + data.accessToken
            },
            data:  {"idkota": id_kota},
            success: function (dataa) {
                rows = dataa.data;
                $('#id_kecamatan').empty();
                $('#id_kecamatan').append("<option value='0' selected> Semua Kecamatan" + '</option>');
    
                if (rows.length > 0) {
                    for (let index = 0; index < rows.length; index++) {
                        if (dataa.id_kecamatan == rows[index].KECAMATAN_ID) {
                            $('#id_kecamatan').append("<option value='"+ rows[index].KECAMATAN_ID+"' selected>" + rows[index].NAMA_KECAMATAN + '</option>');
                        } else {
                            $('#id_kecamatan').append("<option value='"+ rows[index].KECAMATAN_ID+"'>" + rows[index].NAMA_KECAMATAN + '</option>');
                        }
                    }
                    var selectedKecamatan = $('#id_kecamatan').val();
                    getDataKelurahan(selectedKecamatan);
                }
            },
            statusCode: {
                404: function() {
                    console.log( "page not found" );
                }
            }
        })
        .done(function() {
            console.log( "success" );
        })
        .fail(function() {
            console.log( "error" );
        })
        .always(function() {
            console.log( "complete" );
        });
    }
    
    function getDataKelurahan(id_kecamatan) {
        $.ajax({
            type: 'POST',
            url: apiDomain + "master/getDataKelurahan",
            headers: {
                Authorization: 'bearer '+ data.accessToken
            },
                data: {"idkecamatan": id_kecamatan},
                success: function (data) {
                    rows = data.data;
                    $('#id_kelurahan').empty();	
                    // $('#id_kecamatan').append('<option value=""> pilih kelurahan </option>');
                    if (rows.length > 0) {							
                        var selectedKecamatan = $('#id_kecamatan').val();					
                        for (let index = 0; index < rows.length; index++) {
                                if (data.id_kelurahan == rows[index].KELURAHAN_ID && selectedKecamatan != "") {
                                    $('#id_kelurahan').append("<option value='"+ rows[index].KELURAHAN_ID+"' selected>" + rows[index].NAMA_KELURAHAN + '</option>');
    
                                } else {
                                    $('#id_kelurahan').append("<option value='"+ rows[index].KELURAHAN_ID+"'>" + rows[index].NAMA_KELURAHAN + '</option>');
                                }
                            }
                            getRelawanList();
                    }
                    
                },
        
            
            statusCode: {
                404: function() {
                    console.log( "page not found" );
                }
            }
        })
        .done(function() {
            console.log( "success" );
        })
        .fail(function() {
            console.log( "error" );
        })
        .always(function() {
            console.log( "complete" );
        });
    }
    
    function getRelawanList() {
    
        var selectedKota = $('#id_kota').val();
        var selectedKecamatan = $('#id_kecamatan').val();
        var selectedKelurahan = $('#id_kelurahan').val();
    
        // alert(selectedKelurahan);
    
    
        $.ajax({
            type: 'POST',
            // url: apiDomain + 'user/users',
            url: '../api/users.php',
            headers: {
                Authorization: 'bearer ' + data.accessToken
            },
            data: JSON.stringify({
                kota_id: selectedKota, 
                kecamatan_id: selectedKecamatan,
                kelurahan_id: selectedKelurahan
            }),
            success: function (dataa) {
                console.log(dataa);
                if (dataa.data && Array.isArray(dataa.data)) {
                var relawanData = dataa.data;
                console.log(relawanData);
    
                $('#dataTables-example tbody').empty();
                var table = $('#dataTables-example').DataTable();
                table.clear().draw();
    
                relawanData.forEach(function (relawan) {
                    console.log(relawan);
    
                    table.row.add({
                        "no":0,
                        "ID_RELAWAN": relawan.ID_RELAWAN,
                        "NAMA_RELAWAN": relawan.NAMA_RELAWAN,
                        "REGISTER_DATE": relawan.REGISTER_DATE,
                        "NO_TELP": relawan.NO_TELP,
                        "NAMA_KELURAHAN": relawan.NAMA_KELURAHAN,
                        "alamat": relawan.alamat,
    
                        // "ID_RELAWAN": relawan.ID,
                        // "NAMA_RELAWAN": relawan.name,
                        // "REGISTER_DATE": relawan.create_account_date,
                        // "NO_TELP": relawan.phone_number,
                        // "NAMA_KELURAHAN": relawan.village_name,
                        // "alamat": relawan.alamat,
                    }).draw();
                });
    
        table
        .on('order.dt search.dt', function () {
            let i = 1;
     
            table
                .cells(null, 0, { search: 'applied', order: 'applied' })
                .every(function (cell) {
                    this.data(i++);
                });
        })
        .draw();
                $('#dataTables-example .btn-surat').on('click', function () {
    
                    var data_row = table.row($(this).closest('tr')).data();
                    window.open("generate_surat_mandat.php?userId=" + data_row.ID_RELAWAN + "&bearer=" + data.accessToken);
                });
            } else {
                console.error("Invalid or missing data structure in the server response");
            }
        },
            statusCode: {
                404: function () {
                    console.log("page not found");
                }
            }
        })
            .done(function () {
                console.log("success");
            })
            .fail(function (dataerror) {
                console.log(dataerror);
            })
            .always(function () {
                console.log("complete");
            });  
    
    
    
        var table = $('#dataTables-example').DataTable();
    
            $('#dataTables-example tbody').on('click', '.btn-edit', function () {
                var data_row = table.row($(this).closest('tr')).data();
                window.location.href = "form_detail_relawan.html?id=" + data_row.ID_RELAWAN ;
            });
            $('#dataTables-example').on('click', '.btn-delete', function () {
                var data_row = table.row($(this).closest('tr')).data();
    
                $.ajax({
                    type: 'POST',
                    url: '../api/delete_relawan.php',
                    mimeType: 'json',
                    data: JSON.stringify({
                        id: data_row.ID_RELAWAN,
                    }),
                    success: function (data) {
                        alert('Deleted');
                    },
                    statusCode: {
                        404: function() {
                          alert( "page not found" );
                        }
                    }
                })
                .done(function() {
                    console.log( "success" );
                })
                .fail(function(dataerror) {
                    console.log(dataerror);
                })
                .always(function() {
                    console.log( "complete" );
                });
    
            });
            
    
            
                
    };
            
        
    // function deleteRelawan(relawanId) {
    //     $.ajax({
    //         url: 'delete_relawan.php',
    //         type: 'DELETE',
    //         dataType: 'json',
    //         data: JSON.stringify({ id: relawanId }),
    //         success: function(response) {
    //             console.log(response);
    
    //             if (response.status === 200) {
    //                 alert('Data berhasil dihapus');
                    
    //                 location.reload();
    //             } else {
    //                 alert('Gagal menghapus data. Silakan coba lagi.');
    //             }
    //         },
    //         error: function(error) {
    //             console.error(error);
                
    //             alert('Terjadi kesalahan dalam menghapus data.');
    //         }
    //     });
    // }
    
    // var relawanIdToDelete = 123;
    // deleteRelawan(relawanIdToDelete);
    