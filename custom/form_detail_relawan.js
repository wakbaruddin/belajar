$(document).ready(function () {
    var queryString = window.location.search;
    var urlParams = new URLSearchParams(queryString);
    var id = urlParams.get('id');
	kelurahanId = urlParams.get('kelurahanId');
	console.log(id);
    RefreshData(id);
});
var kelurahanId = 0;

function RefreshData(id) {
    $.ajax({
        type: 'GET',
        url: '../api/detail_relawan.php?id=' + id,
        // data: { id: id },
        dataType: 'json',
        success: function (dataa) {
            console.log(dataa);

            $("#id").val(id);
			$("#kelurahanId").val(kelurahanId);
            $("#name").val(dataa.name);
            $("#phone_number").val(dataa.phone_number);
            $("#alamat").val(dataa.alamat);
            $("#email").val(dataa.email);

            // $("#phone_number").prop("disabled", true);
            $("#email").prop("disabled", true);
        },
        statusCode: {
            404: function () {
                alert("page not found");
            }
        }
    })
    .done(function () {
        console.log("success");
    })
    .fail(function () {
        console.log("error");
    })
    .always(function () {
        console.log("complete");
    });
}

function Save() {
    var form = $('#formListRelawan').serialize();
    console.log(form);
    var data = form;
    console.log(data);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: '../api/detail_relawan_save.php',
        data: data,
        contentType: 'application/x-www-form-urlencoded',
        success: function (_response) {
            alert('Saved');
            window.location.href = 'index.html';
        },
        error: function (response) {
            console.log(response);
            alert('Something went Wrong!', 'danger');
        }
    });
}

// function goBack() {
//     window.location.href = "index.html";
// }