$(document).ready(function () {
	var queryString = window.location.search;
	var urlParams = new URLSearchParams(queryString);
	id = urlParams.get('id');
	RefreshData();
});
var id ;
function RefreshData(){
	$.ajax({
        type: 'GET',
        url: '../api/detail_list_tps.php',
        mimeType: 'json',
        data: 'id='+id,
        success: function (data) {

            $("#id").val(id);
            $("#totaltps").val(data.total_tps);
		},
		statusCode: {
			404: function() {
			  alert( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}



function Save(){
	
	var form = $('#formListTPS').serialize();
	console.log(form);
	//var data = new FormData(form);
	var data =form;
	console.log(data);
	$.ajax({
		type: "POST",
		enctype: 'multipart/form-data',
		url: '../api/detail_list_tps_update.php',
		data: data,
		success: function(response) {
			alert('Saved');
		},
		error: function(response) {
			console.log(response);
			alert('Something went Wrong!', 'danger')}
	});
}