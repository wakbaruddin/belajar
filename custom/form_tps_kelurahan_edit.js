$(document).ready(function () {
	var queryString = window.location.search;
	var urlParams = new URLSearchParams(queryString);
	id = urlParams.get('id');
	console.log(id);
	RefreshData();
});
var id ;
function RefreshData(){
	$.ajax({
        type: 'GET',
        url: '../api/detail_tps_kelurahan.php?id=' + id,
        mimeType: 'json',
        data: 'id='+id,
        success: function (data) {
			console.log(data);
            $("#id").val(id);
            $("#nomorTps").val(data.tps_no);
            $("#alamatTps").val(data.tps_addr);
		},
		statusCode: {
			404: function() {
			  alert( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}



function Save(){
	
	var form = $('#formTPSKelurahan').serialize();
	// console.log(form);
	//var data = new FormData(form);
	var data =form;
	// console.log(data);
	$.ajax({
		type: "POST",
		enctype: 'multipart/form-data',
		url: '../api/form_tps_kelurahan_edit.php',
		data: data,
		success: function(response) {
            console.log(response);
			alert('Saved');
		},
		error: function(response) {
			alert('Something went Wrong!', 'danger')}
	});
}