$(document).ready(function () {	
$('#listTPS').DataTable({
	responsive: true,
    pageLength:50,
    sPaginationType: "full_numbers",
    oLanguage: {
        oPaginate: {
            sFirst: "<<",
            sPrevious: "<",
            sNext: ">", 
            sLast: ">>" 
        }
    },
		'searching': true,
		'columns': [
            { data: 'tps_no' },
            { data: 'village_name' },
			{ data: 'tps_name' },
			{ data: 'tps_addr' },
			{ data: 'actions', 
				render: function (data, type, row) {
					return '<button type="button" class="btn btn-primary btn-edit" data-id="' + row.id + '">Edit</button>';
				}
			},
		]
	});


	getTampil_kota(data.id_prov);
	getTPSKelurahan();
	// console.log(data);
	// getTampil_kecamatan(data.id_kota);
	// getTampil_kelurahan(data.id_kecamatan);

});

function onchange_Kota(){
	var selectedKota = $('#id_kota').val();
	getTampil_kecamatan(selectedKota);
}

function onchange_Kecamatan(){
	var selectedKecamatan = $('#id_kecamatan').val();
	getTampil_kelurahan(selectedKecamatan);
}

function onchange_Kelurahan(){
	getTPSKelurahan();
}

function getTampil_kota(id_provinsi){


	$.ajax({
        type: 'POST',
		url: apiDomain + "master/getDataKota",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},
			data: {"idprovinsi": id_provinsi},
			success: function (datas) {
				// console.log(datas);
				rows = datas.data;

				
				$('#id_kota').empty();

				if (rows.length > 0) {
					for (let index = 0; index < rows.length; index++) {					
						if (data.id_kota == rows[index].KOTA_ID) {
							$('#id_kota').append("<option value='"+ rows[index].KOTA_ID+"' selected>" + rows[index].NAMA_KOTA + '</option>');

						} else {
							$('#id_kota').append("<option value='"+ rows[index].KOTA_ID+"'>" + rows[index].NAMA_KOTA + '</option>');
						}
					}
				}	
			},

		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		// console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		var selectedKota = $('#id_kota').val();
		// console.log( "complete" );
		getTampil_kecamatan(selectedKota);
	});
}

function getTampil_kecamatan(id_kota) {
// console.log(id_kota);

	$.ajax({
        type: 'POST',
		url: apiDomain + "master/getDataKecamatan",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},
			data: {"idkota": id_kota},
			success: function (datas) {
				// console.log(datas);
				rows = datas.data;
				
				$('#id_kecamatan').empty();
				
				if (rows.length > 0) {
					for (let index = 0; index < rows.length; index++) {					
						if (data.id_kecamatan == rows[index].KECAMATAN_ID) {
							$('#id_kecamatan').append("<option value='"+ rows[index].KECAMATAN_ID+"' selected>" + rows[index].NAMA_KECAMATAN + '</option>');

						} else {
							$('#id_kecamatan').append("<option value='"+ rows[index].KECAMATAN_ID+"'>" + rows[index].NAMA_KECAMATAN + '</option>');
						}
					}
				}
			},
	
        
		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		// console.log( "success" );
	})
	.fail(function() {
		// console.log( "error" );
	})
	.always(function() {
		// console.log( "complete" );
		var selectedKecamatan = $('#id_kecamatan').val();
		getTampil_kelurahan(selectedKecamatan)
	});
}

function getTampil_kelurahan(id_kecamatan) {
	$.ajax({
        type: 'POST',
		url: apiDomain + "master/getDataKelurahan",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},
			data: {"idkecamatan": id_kecamatan},
			success: function (datas) {
				rows = datas.data;
				$('#id_kelurahan').empty();	
				var selectednamaKelurahan;
				if (rows.length > 0) {							
					var selectedKecamatan = $('#id_kecamatan').val();					
					for (let index = 0; index < rows.length; index++) {
							if (data.id_kelurahan == rows[index].KELURAHAN_ID && selectedKecamatan != "") {
								$('#id_kelurahan').append("<option value='"+ rows[index].KELURAHAN_ID+"' selected>" + rows[index].NAMA_KELURAHAN + '</option>');
								selectednamaKelurahan = rows[index].NAMA_KELURAHAN;
							} else {
								$('#id_kelurahan').append("<option value='"+ rows[index].KELURAHAN_ID+"'>" + rows[index].NAMA_KELURAHAN + '</option>');
							}
						}
				}
			},
	
        
		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		// console.log( "success" );
	})
	.fail(function() {
		// console.log( "error" );
	})
	.always(function() {
		// console.log( "complete" );
		getTPSKelurahan();
	});
}

function getTPSKelurahan(){

	var selectedKota = $('#id_kota').val();
	var selectedKecamatan = $('#id_kecamatan').val();
	var selectedKelurahan = $('#id_kelurahan').val();
	
	// console.log(selectedKelurahan);
	$.ajax({
        type: 'POST',
		// url: apiDomain + "profile/getTPSStatus/" + data.id + "/" + typenya,
		url: '../api/tps_kelurahan.php',
		
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},
			contentType:"application/json",
			data: JSON.stringify({
				kota_id: selectedKota,
				kecamatan_id: selectedKecamatan,
				kelurahan_id: selectedKelurahan
			}),
			success: function (dataa) {
				// console.log(dataa);
					var TPSrelawanData = dataa.data;

				// console.log (TPSrelawanData);
				
				$('#listTPS tbody').empty();

				var table = $('#listTPS').DataTable();
				// Hapus table yang ada
				table.clear().draw();
				
				TPSrelawanData.forEach(function (TPSrel) {
					// console.log(TPSrel);
					table.row.add( {
						"id": TPSrel.ID,
						"tps_no": TPSrel.tps_no,
						"village_name":     TPSrel.village_name,
						"tps_name": TPSrel.tps_name,
						"tps_addr": TPSrel.tps_addr
					} ).draw();
				});

				var table = $('#listTPS').DataTable();

				$('#listTPS tbody').on('click', '.btn-edit', function () {
					var data_row = table.row($(this).closest('tr')).data();
					// console.log(data_row);
					window.location.href = "form_tps_kelurahan_edit.html?id=" + data_row.id ;
				});
				// $('#listTPS').on('click', '.btn-delete', function () {
				// 	var data_row = table.row($(this).closest('tr')).data();
		
				// 	$.ajax({
				// 		type: 'POST',
				// 		url: '../api/delete_relawan.php',
				// 		mimeType: 'json',
				// 		data: JSON.stringify({
				// 			id: data_row.ID_RELAWAN,
				// 		}),
				// 		success: function (data) {
				// 			alert('Deleted');
				// 		},
				// 		statusCode: {
				// 			404: function() {
				// 			  alert( "page not found" );
				// 			}
				// 		}
				// 	})
				// 	.done(function() {
				// 		console.log( "success" );
				// 	})
				// 	.fail(function(dataerror) {
				// 		console.log(dataerror);
				// 	})
				// 	.always(function() {
				// 		console.log( "complete" );
				// 	});
		
				// });
			},
        
		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		// console.log( "success" );
	})
	.fail(function(dataerror) {
		// console.log( dataerror);
		
	})
	.always(function() {
		// console.log( "complete" );
	});
	
}

function add_form() {
	var selectedKelurahan = $('#id_kelurahan').val();
	window.location.href = "../user/form_tps_kelurahan_add.html?kelurahanId=" + selectedKelurahan;
}