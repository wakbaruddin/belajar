$(document).ready(function () {
    getVoteSummary();
});

function getVoteSummary() {
    $.ajax({
        type: 'GET',
        url: apiDomain + 'vote/summaryVoteByState/1?status=0',
        headers: {
            Authorization: 'bearer ' + data.accessToken
        },
        success: function (response) {
            console.log(response);

            var calonList = response.data.tableData;

            // $('#dataTables-example tbody').empty();
			var dataTable = $('#dataTables-example tbody');
            dataTable.empty();

            calonList.forEach(function (calon) {
                var newRow = '<tr>' +
                    '<td class="state-name">' + calon.state_name + '</td>' +
					'<td class="state-code" hidden>' + calon.state_code + '</td>' +
                    '<td>' + calon[1] + '</td>' +
                    '<td>' + calon[2] + '</td>' +
                    '<td>' + calon[3] + '</td>' +
                    '</tr>';

                // $('#dataTables-example tbody').append(newRow);
				dataTable.append(newRow);
            });

            
            // $('#dataTables-example tbody tr').on('click', function () {
            //     var stateName = $(this).find('.state-name').text();
            //     getVoteSummaryByCity(stateName);
			dataTable.on('click', 'tr', function () {
                 var stateCode = $(this).find('.state-code').text();
				 console.log (stateCode);
                getVoteSummaryByCity(stateCode);
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error:", textStatus, errorThrown);
        }
    });
}

function getVoteSummaryByCity(stateCode) {
    
    $.ajax({
        type: 'GET',
        url: 'vote/summaryVoteByCity/' + stateCode + '/1?status=0',
        headers: {
            Authorization: 'bearer ' + data.accessToken
        },
        success: function (response) {
            console.log(response);

            var cityDataTable = $('#cityDataTable tbody');
            cityDataTable.empty();

			if (Array.isArray(response.data.tableData)) {
            response.data.tableData.forEach(function (city) {
                var newRow = '<tr>' +
                    '<td>' + city.city_name + '</td>' +
					'<td>' + city[1] + '</td>' +
                    '<td>' + city[2] + '</td>' +
                    '<td>' + city[3] + '</td>' +
                    '</tr>';
					console.log (newRow);

                cityDataTable.append(newRow);
            });
		} else {
			console.log("Data tidak sesuai.");
		}
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("Error:", textStatus, errorThrown);
        }
    });
}