$(document).ready(function () {

table = $('#listTPS').DataTable(
	{
		'searching': true,
		'columns': [
			{ data: 'state_name' },
			{ data: 'city_name' },
			{ data: 'village_name' },
			{ data: 'total_tps' },
			{ data: 'actions' },
		]
	});
	
	RefreshData();

	getTampil_kota(data.id_prov);
	console.log(data);
	getTampil_kecamatan(data.id_kota);
	getTampil_kelurahan(data.id_kecamatan);

});

var table ;
function RefreshData(){
	$.ajax({
        type: 'POST',
        url: '../api/list_tps.php',
        mimeType: 'json',
        data: '',
        success: function (data) {
			console.log(data);
			var body = "";
            for (var i = 0; i < data.length; i++) {
				data[i].actions=
						// '<td><button type="button" class="btn btn-outline-primary" onclick="window.open(\'form_list_tps_add.html?id='+data[i].ID+'\', \'_blank\'); return false;">Add</button>'+
						'<button type="button" class="btn btn-outline-success" onclick="window.open(\'form_list_tps_edit.html?id='+data[i].ID+'\', \'_blank\'); return false;">Edit</button>' +
						'<button type="button" class="btn btn-outline-danger" onclick="Delete(\''+data[i].ID+'\')">Delete</button></td></tr>';				
				table.row.add(data[i]).draw();
            }
		},
		statusCode: {
			404: function() {
			  alert( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}

function Delete(id){

	$.ajax({
        type: 'GET',
        url: '../api/form_list_tps_delete.php',
        mimeType: 'json',
        data: 'id='+id,
        success: function (data) {
			alert('Deleted');
		},
		statusCode: {
			404: function() {
			  alert( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		alert('fail');
	})
	.always(function() {
		console.log( "complete" );
	});
}

function onchange_Kota(){
	var selectedKota = $('#id_kota').val();
	getTampil_kecamatan(selectedKota);
}

function onchange_Kecamatan(){
	var selectedKecamatan = $('#id_kecamatan').val();
	getTampil_kelurahan(selectedKecamatan);
}

function onchange_Kelurahan(){
	getListTPS();
}

function getTampil_kota(id_provinsi){
	console.log(data);


	$.ajax({
        type: 'POST',
		url: apiDomain + "master/getDataKota",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},
			data: {"idprovinsi": id_provinsi},
			success: function (datas) {
				console.log(datas);
				rows = datas.data;

				
				$('#id_kota').empty();

				if (rows.length > 0) {
					for (let index = 0; index < rows.length; index++) {					
						if (data.id_kota == rows[index].KOTA_ID) {
							$('#id_kota').append("<option value='"+ rows[index].KOTA_ID+"' selected>" + rows[index].NAMA_KOTA + '</option>');

						} else {
							$('#id_kota').append("<option value='"+ rows[index].KOTA_ID+"'>" + rows[index].NAMA_KOTA + '</option>');
						}
					}
				}	

			},

		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}

function getTampil_kecamatan(id_kota) {


	$.ajax({
        type: 'POST',
		url: apiDomain + "master/getDataKecamatan",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},
			data: {"idkota": id_kota},
			success: function (datas) {
				rows = datas.data;
				
				$('#id_kecamatan').empty();
				
				
				// $('#id_kecamatan').append("<option value='"+ datas.KECAMATAN_ID +"' selected> Semua Kecamatan" + '</option>');

				// console.log("<option value='"+ datas.data.KECAMATAN_ID+"' selected> Semua Kecamatan" + '</option>');

				if (rows.length > 0) {
					for (let index = 0; index < rows.length; index++) {					
						if (data.id_kecamatan == rows[index].KECAMATAN_ID) {
							$('#id_kecamatan').append("<option value='"+ rows[index].KECAMATAN_ID+"' selected>" + rows[index].NAMA_KECAMATAN + '</option>');

						} else {
							$('#id_kecamatan').append("<option value='"+ rows[index].KECAMATAN_ID+"'>" + rows[index].NAMA_KECAMATAN + '</option>');
						}
					}
				}
			},
	
        
		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}

function getTampil_kelurahan(id_kecamatan) {
	$.ajax({
        type: 'POST',
		url: apiDomain + "master/getDataKelurahan",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},
			data: {"idkecamatan": id_kecamatan},
			success: function (datas) {
				rows = datas.data;
				$('#id_kelurahan').empty();	
				if (rows.length > 0) {							
					var selectedKecamatan = $('#id_kecamatan').val();					
					for (let index = 0; index < rows.length; index++) {
							if (data.id_kelurahan == rows[index].KELURAHAN_ID && selectedKecamatan != "") {
								$('#id_kelurahan').append("<option value='"+ rows[index].KELURAHAN_ID+"' selected>" + rows[index].NAMA_KELURAHAN + '</option>');

							} else {
								$('#id_kelurahan').append("<option value='"+ rows[index].KELURAHAN_ID+"'>" + rows[index].NAMA_KELURAHAN + '</option>');
							}
						}
				}
			},
	
        
		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}

function getListTPS(){

	var selectedKota = $('#id_kota').val();
	var selectedKecamatan = $('#id_kecamatan').val();
	var selectedKelurahan = $('#id_kelurahan').val();
	
	$.ajax({
        type: 'POST',
		// url: apiDomain + "profile/getTPSStatus/" + data.id + "/" + typenya,
		url: apiDomain + "list/tps",
		headers: {
			Authorization: 'bearer '+ data.accessToken
		},

			data: {
				kota_id: selectedKota,
				kecamatan_id: selectedKecamatan,
				kelurahan_id: selectedKelurahan
			},
			success: function (data) {

				// console.log(data);
					var TPSrelawanData = data.data;

				// console.log (TPSrelawanData);
				
				$('#tpsRelawan tbody').empty();

				var table = $('#tpsRelawan').DataTable();
				// Hapus table yang ada
				table.clear().draw();
				
				TPSrelawanData.forEach(function (TPSrel) {
					table.row.add( {
						"tps_no":   TPSrel.tps_no,
						"village_name":     TPSrel.village_name,
						"district_name": TPSrel.district_name,
						"city_name":	TPSrel.city_name

					} ).draw();
				});
			},
        
		statusCode: {
			404: function() {
			  console.log( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
	
}

function add_form() {
	var selectedProv = data.id_prov;
	var selectedKota = $('#id_kota').val();
	var selectedKelurahan = $('#id_kelurahan').val();
	window.location.href = "../user/form_list_tps_add.html?kelurahanId=" + selectedKelurahan + "&kotaId=" + selectedKota +"&provId=" + selectedProv;

}