$(document).ready(function () {

    $('#listAbsensi').DataTable({
        responsive: false,
        pageLength: 25,
        sPaginationType: "full_numbers",
        oLanguage: {
            oPaginate: {
                sFirst: "<<",
                sPrevious: "<",
                sNext: ">",
                sLast: ">>"
            }
        },
        "columns": [
            { data: "id", title: "ID" },
            { data: "user_id", title: "User ID" },
            { data: "user_name", title: "Nama"},
            { data: "check_in", title: "Masuk kerja" },
            { data: "check_out", title: "Pulang Kerja" },
            { data: 'actions', title: "Actions",
				render: function (data, type, row) {
					// return '<button type="button" class="btn btn-primary btn-edit" data-id="' + row.id + '">Edit Check-in</button>' +
                    return '<button type="button" class="btn btn-primary btn-outline-success" onclick="window.open(\'form_edit_checkin.html?id='+row.id+'\', \'_blank\'); return false;">Edit checkin</button>' + 
                    '<button type="button" class="btn btn-primary btn-edit" onclick="window.open(\'form_edit_checkin.html?id='+row.id+'\', \'_blank\'); return false;">Edit checkout</button>';
				}
			},
        ],
    
    });
    
    getAbsensiList();
    
});
  
    
function getAbsensiList() {

    $.ajax({
        type: 'POST',
        url: '../api/list_absensi.php',
        success: function (dataa) {
            console.log(dataa);
            var Absensi = dataa;
            console.log(Absensi);

            $('#listAbsensi tbody').empty();
            var table = $('#listAbsensi').DataTable();
            table.clear().draw();

            Absensi.forEach(function (absen) {
                console.log(absen);

                table.row.add({
                    "id": absen.id,
                    "user_id": absen.user_id,
                    "user_name": absen.user_name,
                    "check_in": absen.check_in,
                    "check_out": absen.check_out,
                }).draw();
            });
            var table = $('#listAbsensi').DataTable();

				$('#listAbsensi tbody').on('click', '.btn-edit', function () {
					var data_row = table.row($(this).closest('tr')).data();
					// console.log(data_row);
					window.location.href = "form_edit_checkin.html?id=" + data_row.id ;
				});

        },
        statusCode: {
            404: function () {
                console.log("page not found");
            }
        }
    })
    .done(function () {
        console.log("success");
    })
    .fail(function (dataerror) {
        console.log(dataerror);
    })
    .always(function () {
        console.log("complete");
    });  
        
};   