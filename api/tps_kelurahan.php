<?php
//Ref https://makitweb.com/how-to-add-toggle-button-in-datatables-with-jquery-php/
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
include "../config.php";
include "../library/MySQLConnection.php";

	$connection = new mySQLConnection();
	$connection->openCon();
	$content 				= file_get_contents("php://input");
	

	$data    				= json_decode($content, true);

	$kelurahanId			= $data["kelurahan_id"];
	

    $row = $connection->exeQuery("SELECT a.*, l.village_name
	FROM dt_tps a
	LEFT JOIN m_kelurahan l
	on a.kelurahan_id = l.ID  where a.kelurahan_id = $kelurahanId");	
    
	$obj = new stdClass();
	$obj->status = 200;
	$obj->data = $row;

	echo json_encode($obj);
	
?>