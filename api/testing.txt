SELECT u.id ID_RELAWAN,u.name NAMA_RELAWAN,u.phone_number NO_TELP,u.email EMAIL_ADDRESS,u.kelurahan_id KELURAHAN_ID,d.village_code KELURAHAN_CODE,d.village_name NAMA_KELURAHAN,u.image,u.alamat,u.role,u.is_official, DATE_FORMAT(u.created_account_date,"%Y-%m-%d") REGISTER_DATE,u.simpul_relawan
	FROM dt_user u
	join m_kelurahan d 
		on d.id = u.kelurahan_id 
	join m_kecamatan e 
		on d.district_code = e.district_code WHERE d.id = 47107