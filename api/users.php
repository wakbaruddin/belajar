<?php
//Ref https://makitweb.com/how-to-add-toggle-button-in-datatables-with-jquery-php/
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
include "../config.php";
include "../library/MySQLConnection.php";

	$connection = new mySQLConnection();
	$connection->openCon();
	$content 				= file_get_contents("php://input");
	
	$data    				= json_decode($content, true);
	
	$kelurahanId			= $data["kelurahan_id"];

    $row = $connection->exeQuery("SELECT u.id ID_RELAWAN,u.name NAMA_RELAWAN,u.phone_number NO_TELP,u.email EMAIL_ADDRESS,u.kelurahan_id KELURAHAN_ID,d.village_code KELURAHAN_CODE,d.village_name NAMA_KELURAHAN,u.image,u.alamat,u.role,u.is_official, DATE_FORMAT(u.created_account_date,\"%Y-%m-%d\") REGISTER_DATE,u.simpul_relawan
	FROM dt_user u
	join m_kelurahan d 
		on d.id = u.kelurahan_id 
	join m_kecamatan e 
		on d.district_code = e.district_code WHERE d.id = $kelurahanId");
    
	$obj = new stdClass();
	$obj->status = 200;

	$myfile = fopen("testing.txt", "w") or die("Unable to open file!");
	$txt = "John Doe\n";
	fwrite($myfile, "SELECT u.id ID_RELAWAN,u.name NAMA_RELAWAN,u.phone_number NO_TELP,u.email EMAIL_ADDRESS,u.kelurahan_id KELURAHAN_ID,d.village_code KELURAHAN_CODE,d.village_name NAMA_KELURAHAN,u.image,u.alamat,u.role,u.is_official, DATE_FORMAT(u.created_account_date,\"%Y-%m-%d\") REGISTER_DATE,u.simpul_relawan
	FROM dt_user u
	join m_kelurahan d 
		on d.id = u.kelurahan_id 
	join m_kecamatan e 
		on d.district_code = e.district_code WHERE d.id = $kelurahanId");
	fclose($myfile);

	$obj->data = $row;

	
		echo json_encode($obj);
?>