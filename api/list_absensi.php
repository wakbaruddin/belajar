<?php
//Ref https://makitweb.com/how-to-add-toggle-button-in-datatables-with-jquery-php/
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
include "../config.php";
include "../library/MySQLConnection.php";

	$connection = new mySQLConnection();
	$connection->openCon();
	
    $row = $connection->exeQuery("SELECT a.*, n.user_name
	FROM r_absen a
	LEFT JOIN m_karyawan n
	on a.user_id = n.user_id");
    
	echo json_encode($row);
	
?>