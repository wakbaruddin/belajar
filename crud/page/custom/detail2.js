$(document).ready(function () {
	var queryString = window.location.search;
	var urlParams = new URLSearchParams(queryString);
	id = urlParams.get('id');
	RefreshData();
});
var id ;
function RefreshData(){
	$.ajax({
        type: 'GET',
        url: '../api/detail2.php',
        mimeType: 'json',
        data: 'id='+id,
        success: function (data) {
			$("#id").val(id);
			$("#name").val(data.name);
			$("#description").val(data.description);
		},
		statusCode: {
			404: function() {
			  alert( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}

function Save(){
	var form = $('#myForm')[0];
	var data = new FormData(form);
	$.ajax({
		type: "POST",
		enctype: 'multipart/form-data',
		url: '../api/detail2-save.php',
		data: data,
		processData: false,
		contentType: false,
		cache: false,
		timeout: 800000,
		success: function(response) {
			alert('Saved');
		},
		error: function(response) {
			// console.log(response)
			notify('Something went Wrong!', 'danger')
		}
	});
}