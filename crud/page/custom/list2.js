$(document).ready(function () {
    table = $('#example').DataTable(
	{
		'searching': true,
		'columns': [
			{ data: 'name' },
			{ data: 'description' },
			{ data: 'actions' },
		]
	});
	
	
	
	RefreshData();
	
	
});
$('#example tbody').on('click', 'button', function () {
        var data = table.row($(this).parents('tr')).data();
        alert(data[0] + "'s salary is: " + data[5]);
    });
var table ;
function RefreshData(){
	$.ajax({
        type: 'POST',
        url: '../api/list2.php',
        mimeType: 'json',
        data: '',
        success: function (data) {
			console.log(data);
			var body = "";
            for (var i = 0; i < data.length; i++) {
				data[i].actions='<td><button type="button" class="btn btn-outline-primary" onclick="window.open(\'detail2.html?id=-\', \'_blank\'); return false;">Add</button>'+
						'<button type="button" class="btn btn-outline-success" onclick="window.open(\'detail2.html?id='+data[i].id+'\', \'_blank\'); return false;">Edit</button>'+
						'<button type="button" class="btn btn-outline-danger" onclick="Delete(\''+data[i].id+'\')">Delete</button></td></tr>';
				
				table.row.add(data[i]).draw();
            }
            //$("#example tbody").append(body);
		},
		statusCode: {
			404: function() {
			  alert( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		console.log( "error" );
	})
	.always(function() {
		console.log( "complete" );
	});
}

function Delete(id){

	$.ajax({
        type: 'GET',
        url: '../api/detail2-delete.php',
        mimeType: 'json',
        data: 'id='+id,
        success: function (data) {
			alert('Deleted');
		},
		statusCode: {
			404: function() {
			  alert( "page not found" );
			}
		}
	})
	.done(function() {
		console.log( "success" );
	})
	.fail(function() {
		alert('fail');
	})
	.always(function() {
		console.log( "complete" );
	});
}