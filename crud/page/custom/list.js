$(document).ready(function () {
    table = $('#example').DataTable(
	{
		'processing': true,
		'serverSide': true,
		'serverMethod': 'post',
		'searching': true, // Set false to Remove default Search Control
		'ajax': {
			'url':'../api/list.php',
			'data': function(data){
				// Append to data
				data.request = "paginationData";
			},
			statusCode: {
				404: function() {
					alert( "page not found" );
				}
			}
		},
		'columns': [
			{ data: 'name' },
			{ data: 'description' },
			{ data: 'actions' },
		]
	});
	
	
	
	//RefreshData();
	
	
});
$('#example tbody').on('click', 'button', function () {
        var data = table.row($(this).parents('tr')).data();
        alert(data[0] + "'s salary is: " + data[5]);
    });
var table ;