<?php
//Ref https://makitweb.com/how-to-add-toggle-button-in-datatables-with-jquery-php/
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
include "../config.php";
include "../library/MySQLConnection.php";

	$connection = new mySQLConnection();
	$connection->openCon();
	$id = $_GET['id'];
    $row = $connection->exeQuery("SELECT * FROM items WHERE id = $id");
    
	if(count($row)>0)
		echo json_encode($row[0]);
	else
		echo '{}';
?>