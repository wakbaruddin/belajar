<?php
//Ref https://makitweb.com/how-to-add-toggle-button-in-datatables-with-jquery-php/
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
include "../config.php";
include "../library/MySQLConnection.php";

	$request = "";
	if(isset($_POST['request'])){
		 $request = $_POST['request'];
	}
	
	$draw = $_POST['draw'];
	$start = $_POST['start'];
	$rowperpage = $_POST['length']; // Rows display per page
	$columnIndex = $_POST['order'][0]['column']; // Column index
	$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
	$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
	$searchValue = $_POST['search']['value']; // Search value
	 ## Search 
	$searchQuery = " ";
	if($searchValue != ''){
		$searchQuery = " AND (name LIKE '%".$searchValue."%' OR description LIKE '%".$searchValue."%') ";
	}
	 
	$connection = new mySQLConnection();
	$connection->openCon();
	
    $row = $connection->exeQuery("SELECT COUNT(*) as allcount FROM items");
    $totalRecords = intval($row[0]['allcount']);

	$row = $connection->exeQuery("SELECT COUNT(*) as allcount FROM items WHERE 1 ".$searchQuery);
	$totalRecordwithFilter = intval($row[0]['allcount']);
	
	//$rows = $connection->exeQuery("SELECT name,description FROM items");
	$rowsFilter = $connection->exeQuery("SELECT id,name,description FROM items WHERE 1 ".$searchQuery." ORDER BY ".$columnName." ".$columnSortOrder." LIMIT ".$start.",".$rowperpage);
	
	foreach($rowsFilter as &$rowFilter){
		$id = $rowFilter['id'];
		$action = "<button type=\"button\" class=\"btn btn-outline-primary\" onclick=\"window.open('detail.html?id=-', '_blank'); return false;\">Add</button>
		<button type=\"button\" class=\"btn btn-outline-primary\" onclick=\"window.open('detail.html?id=$id', '_blank'); return false;\">Edit</button>
		<button type=\"button\" class=\"btn btn-outline-primary\" onclick=\"window.open('delete.html?id=$id', '_blank'); return false;\">Delete</button>";
		  $rowFilter['actions']= $action;
	}
	
	$response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $rowsFilter
     );

	echo json_encode($response);
	die;
?>