<?php 

class mySQLConnection{
    public $con;
    private $dataAdapter;
    private $row;
    function openCon(){
        if($GLOBALS['waDBType']=="MYSQL")
            $this->con = mysqli_connect($GLOBALS['waHost'], $GLOBALS['waUser'], $GLOBALS['waPass'], $GLOBALS['waDatabaseName'])or die('Error connecting to DB');
        else if($GLOBALS['waDBType']=="SQLSVR"){
            $con = mssql_connect($GLOBALS['waHost'], $GLOBALS['waUser'], $GLOBALS['waPass']);
            mssql_selectdbType($GLOBALS['waDatabaseName'], $con);
        }else{

         $con = pg_connect("waHost=".$GLOBALS['waHost']." dbname=".$GLOBALS['waDatabaseName']." waUser=".$GLOBALS['waUser']." waPassword=".$GLOBALS['waPass']." port=".$GLOBALS['waPort']);

		}
	}

	function exeQuery($sql){
		if($GLOBALS['waDBType']=="MYSQL"){
			
			try {
				$dataAdapter = mysqli_query($this->con, $sql);
				
				$array = array();
				if($dataAdapter){
					while($d = mysqli_fetch_assoc($dataAdapter)){
						$array[] = $d;
					}
				}
			} catch (Exception $e) {
				echo $e->getMessage();
				die();
			}
		}
		else if($GLOBALS['waDBType']=="SQLSVR"){
			$sql=str_replace("now()","getdate()",$sql);
			$dataAdapter = mssql_query ($sql, $con);
			$array = array();
			//while($d = mssql_fetch_array($dataAdapter)){
				$array[] = mssql_fetch_array($dataAdapter,MYSQLI_ASSOC);
			//}
			
		}
		else{
			$dataAdapter = pg_query($sql);
			//$row = pg_fetch_array($dataAdapter);
			$array = array();
			while($d = pg_fetch_assoc($dataAdapter)){
				$array[] = $d;
			}
			

		}
		return $array;
	}

	function exeNonQuery($sql){
		if($GLOBALS['waDBType']=="MYSQL"){
			$dataAdapter = $this->con->query($sql);
		}
		else if($GLOBALS['waDBType']=="SQLSVR"){
			$sql=str_replace("now()","getdate()",$sql);
			$dataAdapter = mssql_query ($sql, $con);
		}
		else{
			$dataAdapter = pg_query($sql);
		}

	}

	function readDB(){
		if($GLOBALS['waDBType']=="MYSQL"){
			$row=mysqli_fetch_assoc($dataAdapter);
		}
		else if($GLOBALS['waDBType']=="SQLSVR"){
			$row = mssql_fetch_array($dataAdapter);
		}else{
			$row = pg_fetch_array($dataAdapter);
		}

		return $row;
	}
}
/*

function openCon(){
    if($GLOBALS['waDBType']=="MYSQL")
        $con = new mysqli($GLOBALS['waHost'], $GLOBALS['waUser'], $GLOBALS['waPass'], $GLOBALS['waDatabaseName']);
    else if($GLOBALS['waDBType']=="SQLSVR"){
        $con = mssql_connect($GLOBALS['waHost'], $GLOBALS['waUser'], $GLOBALS['waPass']);
               mssql_selectwaDBType($GLOBALS['waDatabaseName'], $con);
    }else{
       $con = pg_connect("waHost=localwaHost dbname=db_bdi waUser=postgres waPassword=fid123!! port=5432");
       
    }
    return $con;
   
}

function exeQuery($con,$sql){
    if($GLOBALS['waDBType']=="MYSQL"){
        $dataAdapter = $con->query($sql);
    }
    else if($GLOBALS['waDBType']=="SQLSVR"){
        $sql=str_replace("now()","getdate()",$sql);
        $dataAdapter = mssql_query ($sql, $con);
    }
    else{
        $dataAdapter = pg_query($sql);
    }
	////mysqli_close($con);
    return $dataAdapter;
}

function readDB($dataAdapter){
     if($GLOBALS['waDBType']=="MYSQL"){
           $row=mysqli_fetch_array($dataAdapter);
     }
     else if($GLOBALS['waDBType']=="SQLSVR"){
           $row = mssql_fetch_array($dataAdapter);
     }else{
           $row = pg_fetch_array($dataAdapter);
     }
      
    return $row;
}

*/
?>